package id.agunggum.githubuseragung

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import id.agunggum.githubuseragung.databinding.ActivityMainBinding
import id.agunggum.githubuseragung.model.User

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val users = ArrayList<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        "Github Users".setActionBarTitle()
        binding.rvUser.setHasFixedSize(true)
        binding.rvUser.layoutManager = LinearLayoutManager(this)
        users.addAll(listUsers)

        showListUsers()
    }

    private val listUsers: ArrayList<User>
        get() {
            val username = resources.getStringArray(R.array.username)
            val name = resources.getStringArray(R.array.name)
            val avatar = resources.obtainTypedArray(R.array.avatar)
            val location = resources.getStringArray(R.array.location)
            val company = resources.getStringArray(R.array.company)
            val repository = resources.getStringArray(R.array.repository)
            val followers = resources.getStringArray(R.array.followers)
            val following = resources.getStringArray(R.array.following)
            val users = ArrayList<User>()
            for (i in username.indices) {
                val user = User(
                    "${username[i]}",
                    name[i],
                    avatar.getResourceId(i, -1),
                    location[i],
                    company[i],
                    repository[i],
                    followers[i],
                    following[i]
                )
                users.add(user)
            }
            return users
        }

    private fun showListUsers() {
        val listUserAdapter = ListUserAdapter(users)
        binding.rvUser.adapter = listUserAdapter
        listUserAdapter.setOnItemClickCallback(object : ListUserAdapter.OnItemClickCallback{
            override fun onItemClicked(data: User) {
                detailUser(data)
            }
        })
    }

    private fun detailUser(user: User) {
        val intent = Intent(this@MainActivity, UserDetailActivity::class.java)
        intent.putExtra(UserDetailActivity.EXTRA_USER, user)
        startActivity(intent)
    }

    private fun String.setActionBarTitle() {
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).title = this
        }
    }

}