package id.agunggum.githubuseragung

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import id.agunggum.githubuseragung.databinding.ActivityUserDetailBinding
import id.agunggum.githubuseragung.model.User

class UserDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUserDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val user = intent.getParcelableExtra<User>(EXTRA_USER) as User

        val actionBar = supportActionBar
        actionBar!!.title = user.name
        actionBar.setDisplayHomeAsUpEnabled(true)

        binding.tvName.text = user.name
        binding.tvUsername.text = user.username
        binding.tvCompany.text = user.company
        binding.tvLocation.text = user.location
        "Repository ${user.repository}".also { binding.tvRepository.text = it }
        "Followers ${user.followers}".also { binding.tvFollowers.text = it }
        "Following ${user.following}".also { binding.tvFollowing.text = it }
        Glide.with(this)
            .load(user.avatar)
            .into(binding.ivAvatar)
        binding.fabShare.setOnClickListener {
           share(user.username)
        }
    }

    private fun share(username : String){
        val intent= Intent()
        intent.action=Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_TEXT,"For more information please follow @$username")
        intent.type="text/plain"
        startActivity(Intent.createChooser(intent,"Share To:"))
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    companion object {
        const val EXTRA_USER = "extra_user"
    }
}